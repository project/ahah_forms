Examples that show how to use the AHAH Forms Framework

poll.module.example
	To use this module, rename it to poll.module, and drop it into your drupal/modules/poll directory. 
	Probably a good idea to first rename the existing modules/poll/poll.module to poll.module.bak so you can go back.
	
widget & test
	widget is an example of a field element created using the AHAH Forms Framework. 
	You can add it multiple times to the same form.  test.module is an example form that uses it.
	Just enable both modules, and then create a new test content type.