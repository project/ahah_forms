var Ahah = Ahah || {};

/**
 *  element object definition
 *  id: id attribute of the element
 *  event: the type of event being bound to (click, change, etc)
 *  form_id: encapslating form
 *  path: the Drupal uri to call to get the updating html
 *  wrapper: the id of the container to insert the updated html into
 *  params: hash of params to be send back to the uri (overwrites form values)
 */
 
/**
 * Do an Asynchronous call back to Drupal to regenerate the wrapper and swap out the old content.
 */
Ahah.update = function( element ) {
	console.log( "Updating " + element.id + ", " + element.event );
//	return false;
	
	$(this).attr("style", "cursor:wait");
	
	// build the params to post back to drupal as array of objects
    var params = $( '#' + element.form_id ).formToArray( true ); // collect the form's parameters
	for( var key in element.params ) {
//		params[key] = element.params[key];
	    params.push( {name: key, value: element.params[key]} );
	} 
    
    basePath = Drupal.settings.ahah.basePath[0];
    uri = basePath + element.path;
	wrapper_id = '#' + element.wrapper;
		
	$.post( uri, params, function(data) {
		$(wrapper_id).html( data ); // replace the old content
//		$(wrapper_id).highlightFade();
		$(wrapper_id).Highlight(1000, '#ff8'); // yellow flash
		Ahah.attach_wrapper_bindings( element.wrapper ); // re-attach event listeners to newly generated html
		$(this).attr("style", "cursor:auto");
	} );
}

Ahah.attach_to_element = function(element) {
	if( element.class ) {
		key = element.class;
	}
	else {
		key = '#' + element.id;
	}
	console.log( "Attaching to:  " + key + " : " + element.event );
	if( element.event == 'click' ) {
		$(key).click( function() {Ahah.update( element ); return false;} );
	}
	else if( element.event == 'change' ) {
		$(key).change( function() {Ahah.update( element ); return false;} );
	}
	//$(id).bind( element.event, element, Ahah.update ); jquery 1.1 version
} 

Ahah.attach_wrapper_bindings = function( wrapper ) {
	// Drupal.ahah.elements is an array of arrays of elements
	for (var i in Drupal.settings.ahah.bindings ) {
		for (var j in Drupal.settings.ahah.bindings[i] ) {
  			element = Drupal.settings.ahah.bindings[i][j];
  			if( element.wrapper == wrapper ) {
  				Ahah.attach_to_element( element );
  			}
  		}
  	}
}

/**
 *  Attach listeners to all elements
 */
Ahah.attach_all_bindings = function() {
	// Drupal.ahah.elements is an array of arrays of elements
	for (var i in Drupal.settings.ahah.bindings ) {
		for (var j in Drupal.settings.ahah.bindings[i] ) {
  			element = Drupal.settings.ahah.bindings[i][j];
  			Ahah.attach_to_element( element );
  		}
  	}
}

if( Drupal.jsEnabled ) {
	$(document).ready(Ahah.attach_all_bindings);
}